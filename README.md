# TripleCamel
A simple Android app to check Amazon price history on CamelCamelCamel.

This app works by sharing a product from the Amazon app, and by choosing Triple Camel as the sharing app from the list.

This app is not affiliated with CamelCamelCamel. For more information, visit their <a href="http://camelcamelcamel.com/">website</a>.

This app is developed by Ermanno Baschiera (ebaschiera@gmail.com). This software is GPLv2.
